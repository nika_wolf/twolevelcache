package com.vvolkova.twolevelcache.strategy;

public enum CacheStrategyType {
    LRU,
    MRU
}
