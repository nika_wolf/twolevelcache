package com.vvolkova.twolevelcache.strategy;

import com.vvolkova.twolevelcache.cache.CachedObject;

import java.io.Serializable;
import java.util.Map;


public interface CacheStrategy<K extends Serializable, V extends Serializable> {

    Map<K, CachedObject<K, V>> getMap(int capacity);
}
