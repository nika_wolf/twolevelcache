package com.vvolkova.twolevelcache.strategy;

import com.vvolkova.twolevelcache.cache.CachedObject;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractCacheStrategy<K extends Serializable, V extends Serializable> implements CacheStrategy<K,V>{

    protected abstract boolean removeExcessEntry(Map.Entry<K, CachedObject<K, V>> entry);

    public Map<K, CachedObject<K, V>> getMap(int capacity) {
        return Collections.synchronizedMap(new LinkedHashMap<K, CachedObject<K, V>>(capacity, 1, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, CachedObject<K, V>> eldest) {
                return removeExcessEntry(eldest);
            }
        });
    }
}
