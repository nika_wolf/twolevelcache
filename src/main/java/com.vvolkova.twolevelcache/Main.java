package com.vvolkova.twolevelcache;

import com.vvolkova.twolevelcache.cache.cachelevel.TwoLevelCache;
import com.vvolkova.twolevelcache.strategy.CacheStrategyType;

public class Main {
    public static void main(String[] args) {
        TwoLevelCache<String, String> cache = new TwoLevelCache<>(2,3, CacheStrategyType.LRU);
        cache.put("key1", "TEST_A");
        cache.put("key2", "TEST_B");
        System.out.println(cache);
        cache.get("key1");
        cache.get("key2");
        cache.get("key1");
        cache.get("key2");
        cache.put("key3", "TEST_C");
        cache.put("key4", "TEST_D");
        cache.get("key3");
        cache.put("key5", "TEST_E");
        System.out.println(cache);
    }
}
