package com.vvolkova.twolevelcache.cache.cachelevel;

import com.vvolkova.twolevelcache.cache.Cache;
import com.vvolkova.twolevelcache.strategy.CacheStrategyType;

import java.io.Serializable;

public class TwoLevelCache<K extends Serializable, V extends Serializable> implements Cache<K, V> {
    private Cache<K, V> memoryCache;
    private Cache<K, V> fileSystemCache;
    private CacheStrategyType strategy;

    public TwoLevelCache(final int memoryCapacity, final int fileCapacity, final CacheStrategyType strategy) {
        this.strategy = strategy;
        initCaches(memoryCapacity, fileCapacity);
    }

    public Cache<K, V> getMemoryCache() {
        return memoryCache;
    }

    public Cache<K, V> getFileSystemCache() {
        return fileSystemCache;
    }

    private void initCaches(final int memoryCapacity, final int fileCapacity) {
        memoryCache = new MemoryCache<K, V>(strategy, memoryCapacity) {
            @Override
            protected boolean getRemoveExcessEntryCondition() {
                return memoryCache.getSize() > memoryCapacity;
            }

            @Override
            protected void additionalAction(K key) {
                fileSystemCache.put(key, memoryCache.get(key));
            }
        };
        fileSystemCache = new FileSystemCache<>(strategy, fileCapacity);
    }

    @Override
    public V get(K key) {
        V val = memoryCache.get(key);
        if (val == null) {
            val = fileSystemCache.get(key);
            if (val != null) {
                memoryCache.put(key, val);
                fileSystemCache.remove(key);
            }
        }
        return val;
    }

    @Override
    public void put(K key, V val) {
        memoryCache.put(key, val);
    }

    @Override
    public int getSize() {
        return memoryCache.getSize() + fileSystemCache.getSize();
    }

    @Override
    public boolean containsKey(K key) {
        return memoryCache.containsKey(key) || fileSystemCache.containsKey(key);
    }

    @Override
    public boolean remove(K key) {
        return memoryCache.remove(key) && fileSystemCache.remove(key);
    }

    @Override
    public String toString() {
        return "TwoLevelCache{level1{" + memoryCache + "}, level2{" + fileSystemCache + "}}";
    }
}
