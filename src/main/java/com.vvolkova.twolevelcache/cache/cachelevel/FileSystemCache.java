package com.vvolkova.twolevelcache.cache.cachelevel;

import com.vvolkova.twolevelcache.cache.Cache;
import com.vvolkova.twolevelcache.cache.CachedObject;
import com.vvolkova.twolevelcache.strategy.CacheStrategyType;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.UUID;

public class FileSystemCache<K extends Serializable, V extends Serializable> implements Cache<K, V> {
    private static final Logger logger = LoggerFactory.getLogger(FileSystemCache.class);
    private static final String CACHE_DIR = "cache";
    private static final String FILE_EXTENSION = ".dat";
    private Cache<K, String> fileNameStorage;


    public FileSystemCache(CacheStrategyType strategy, int capacity) {
        logger.debug("File system cache strategy: " + strategy);
        logger.debug("File system cache capacity: " + capacity);
        initFileNameStorage(strategy, capacity);
        initDirectory();
        logger.debug("File system cache created");
    }

    private void initFileNameStorage(CacheStrategyType strategy, int capacity) {
        this.fileNameStorage = new MemoryCache<K, String>(strategy, capacity) {
            @Override
            protected void additionalAction(K key) {
                deleteFile(key);
            }
        };
    }

    private void initDirectory() {
        logger.debug("TwoLevelCache filesystem disk path: " + CACHE_DIR);
        File directory = new File(CACHE_DIR);
        if (directory.exists()) {
            try {
                FileUtils.cleanDirectory(directory);
            } catch (IOException e) {
                logger.error("Cannot clean directory: " + CACHE_DIR);
            }
        } else {
            directory.mkdirs();
        }
    }

    protected void deleteFile(K key) {
        logger.debug("Going to delete file for key: {}", key);
        String fileName = fileNameStorage.get(key);
        try {
            Files.deleteIfExists(new File(fileName).toPath());
            logger.debug("Deleted file: " + fileName);
        } catch (IOException e) {
            logger.error("Cannot delete file: " + fileName);
        }
    }

    @Override
    public V get(K key) {
        logger.debug("Going to get object with key: {}", key);
        V val = null;
        if (fileNameStorage.containsKey(key)) {
            String fulFilePath = fileNameStorage.get(key);
            File file = new File(fulFilePath);
            CachedObject<K, V> cachedObject = readObject(file);
            val = cachedObject.getVal();
            logger.debug("Got object, value: {}", val);
        } else {
            logger.debug("There is no such object with key: {}", key);
        }
        return val;
    }

    @Override
    public void put(K key, V val) {
        String fileName = UUID.randomUUID().toString();
        logger.debug("Going to add object");
        String fulFilePath = CACHE_DIR + File.separator + fileName + FILE_EXTENSION;
        File file = new File(fulFilePath);
        CachedObject<K, V> cachedObject = new CachedObject<>(key, val);
        if (cachedObject.isValid()) {
            fileNameStorage.put(key, fulFilePath);
            writeObject(cachedObject, file);
            logger.debug("Added object: {}", cachedObject);
        } else {
            logger.error("Object {} is not valid", cachedObject);
        }
    }

    private void writeObject(Object object, File file) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(
                new FileOutputStream(file)))) {
            oos.writeObject(object);
            oos.flush();
        } catch (Exception e) {
            logger.error("Cannot write object {} to file {}", object, file.getName());
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T readObject(File file) {
        T object = null;
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object obj = ois.readObject();
            object = (T) obj;
        } catch (Exception e) {
            logger.error("Cannot read object from file {}", file.getName());
        }
        return object;
    }

    @Override
    public int getSize() {
        return fileNameStorage.getSize();
    }

    @Override
    public boolean containsKey(K key) {
        return fileNameStorage.containsKey(key);
    }

    @Override
    public boolean remove(K key) {
        deleteFile(key);
        return fileNameStorage.remove(key);
    }

    @Override
    public String toString() {
        return fileNameStorage.toString();
    }
}
