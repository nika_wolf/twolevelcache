package com.vvolkova.twolevelcache.cache.cachelevel;

import com.vvolkova.twolevelcache.cache.Cache;
import com.vvolkova.twolevelcache.cache.CachedObject;
import com.vvolkova.twolevelcache.strategy.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

public class MemoryCache<K extends Serializable, V extends Serializable> implements Cache<K, V> {
    private static final Logger logger = LoggerFactory.getLogger(MemoryCache.class);
    private Map<K, CachedObject<K, V>> objectsStorage;
    private int capacity;

    public MemoryCache(CacheStrategyType cacheStrategyType, int maxSize) {
        CacheStrategy<K, V> cacheStrategy = initCacheStrategy(cacheStrategyType);
        logger.debug("Memory cache strategy: " + cacheStrategyType.name());
        this.capacity = maxSize;
        logger.debug("Memory cache capacity: " + capacity);
        this.objectsStorage = cacheStrategy.getMap(capacity);
        logger.debug("Memory cache created");
    }

    protected CacheStrategy<K, V> initCacheStrategy(CacheStrategyType cacheStrategyType) {
        switch (cacheStrategyType) {
            case LRU:
                return new AbstractCacheStrategy<K, V>() {
                    @Override
                    protected boolean removeExcessEntry(Map.Entry<K, CachedObject<K, V>> entry) {
                        if (getRemoveExcessEntryCondition()) {
                            additionalAction(entry.getKey());
                            logger.debug("Removed object, key: {}", entry.getKey());
                            return true;
                        }
                        return false;
                    }
                };
            case MRU:
                return new AbstractCacheStrategy<K, V>() {
                    @Override
                    protected boolean removeExcessEntry(Map.Entry<K, CachedObject<K, V>> entry) {
                        if (getRemoveExcessEntryCondition()) {
                            K key = getMruKey();
                            additionalAction(key);
                            objectsStorage.remove(key);
                            logger.debug("Removed object, key: {}", key);
                        }
                        return false;
                    }
                };
        }
        return null;
    }

    protected boolean getRemoveExcessEntryCondition() {
        return objectsStorage.size() > capacity;
    }

    protected void additionalAction(K key) {
    }

    private K getMruKey() {
        K key = null;
        Iterator<K> iterator = objectsStorage.keySet().iterator();
        for (int i = 0; i < objectsStorage.size() - 1; i++) {
            key = iterator.next();
        }
        return key;
    }

    @Override
    public V get(K key) {
        logger.debug("Going to get object with key: {}", key);
        V val = null;
        if (objectsStorage.containsKey(key)) {
            val = objectsStorage.get(key).getVal();
            logger.debug("Got object, value: {}", val);
        } else {
            logger.debug("There is no such object with key: {}", key);
        }
        return val;
    }

    @Override
    public void put(K key, V val) {
        logger.debug("Going to add object");
        CachedObject<K, V> cachedObject = new CachedObject<>(key, val);
        if (cachedObject.isValid()) {
            objectsStorage.put(key, cachedObject);
            logger.debug("Added object: {}", cachedObject);
        } else {
            logger.error("Object {} is not valid", cachedObject);
        }
    }

    @Override
    public int getSize() {
        return objectsStorage.size();
    }

    @Override
    public boolean containsKey(K key) {
        return objectsStorage.containsKey(key);
    }

    @Override
    public boolean remove(K key) {
        return objectsStorage.remove(key) != null;
    }

    @Override
    public String toString() {
        return objectsStorage.toString();
    }
}
