package com.vvolkova.twolevelcache.cache;

public interface Cache<K, V> {
    V get(K key);

    void put(K key, V val);

    int getSize();

    boolean containsKey(K key);

    boolean remove(K key);
}
