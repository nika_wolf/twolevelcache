package com.vvolkova.twolevelcache;

import com.vvolkova.twolevelcache.strategy.CacheStrategyType;
import com.vvolkova.twolevelcache.cache.cachelevel.MemoryCache;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MemoryCacheTest {
    private static final int CAPACITY = 3;
    private MemoryCache<String, String> memoryCache;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void putNullKeyShouldTestExceptionMessage() throws IllegalArgumentException {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Null key is not allowed");
        memoryCache.put(null, "TEST");
    }

    @Test
    public void putNullValShouldTestExceptionMessage() throws IllegalArgumentException {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Null value is not allowed");

        memoryCache.put("key1", null);
    }

    @Test
    public void testPut() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "BBB");
        memoryCache.put("key3", "CCC");

        assertEquals(CAPACITY, memoryCache.getSize());
    }

    @Test
    public void testDeletedExcess() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");
        memoryCache.put("key3", "test_3");
        memoryCache.put("key4", "DDD");

        assertEquals(CAPACITY, memoryCache.getSize());
    }

    @Test
    public void testLruStrategy() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");
        memoryCache.put("key3", "test_3");
        memoryCache.put("key4", "DDD");

        assertTrue(!memoryCache.containsKey("key1"));
    }

    @Test
    public void testMruStrategy() {
        CacheStrategyType strategy = CacheStrategyType.MRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");
        memoryCache.put("key3", "test_3");
        memoryCache.put("key4", "test_4");

        assertTrue(!memoryCache.containsKey("key3"));
    }


    @Test
    public void testGet() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");

        assertEquals("test_2", memoryCache.get("key2"));
    }

    @Test
    public void testGetNullKey() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");

        assertEquals(null, memoryCache.get(null));
    }

    @Test
    public void testContainsKeyTrue() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");

        assertTrue(memoryCache.containsKey("key1"));
    }

    @Test
    public void testContainsKeyFalse() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");

        assertTrue(!memoryCache.containsKey("key3"));
    }

    @Test
    public void testGetSize() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");

        assertEquals(2, memoryCache.getSize());
    }

    @Test
    public void testRemove() {
        CacheStrategyType strategy = CacheStrategyType.LRU;
        memoryCache = new MemoryCache<>(strategy, CAPACITY);

        memoryCache.put("key1", "test_1");
        memoryCache.put("key2", "test_2");
        memoryCache.remove("key1");

        assertTrue(!memoryCache.containsKey("key1"));
    }
}