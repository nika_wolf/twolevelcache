package com.vvolkova.twolevelcache;

import com.vvolkova.twolevelcache.cache.cachelevel.FileSystemCache;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;

import static com.vvolkova.twolevelcache.strategy.CacheStrategyType.LRU;
import static com.vvolkova.twolevelcache.strategy.CacheStrategyType.MRU;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileSystemCacheTest {
    private static final int CAPACITY = 3;
    private static final String DIR = "cache";
    private FileSystemCache<String, String> fileSystemCache;
    private File dir = new File(DIR);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void putNullKeyShouldTestExceptionMessage() throws IllegalArgumentException {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Null key is not allowed");
        fileSystemCache.put(null, "TEST");
    }

    @Test
    public void putNullValShouldTestExceptionMessage() throws IllegalArgumentException {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Null value is not allowed");
        fileSystemCache.put("key1", null);
    }

    @Test
    public void testPut() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");
        fileSystemCache.put("key3", "test3");

        assertEquals(CAPACITY, fileSystemCache.getSize());
        assertEquals(CAPACITY, dir.listFiles().length);
    }

    @Test
    public void testDeletedExcess() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");
        fileSystemCache.put("key3", "test3");
        fileSystemCache.put("key4", "test4");

        assertEquals(CAPACITY, fileSystemCache.getSize());
        assertEquals(CAPACITY, dir.listFiles().length);
    }

    @Test
    public void testLruStrategy() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");
        fileSystemCache.put("key3", "test3");
        fileSystemCache.put("key4", "test4");

        assertTrue(!fileSystemCache.containsKey("key1"));
    }

    @Test
    public void testMruStrategy() {
        fileSystemCache = new FileSystemCache<>(MRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");
        fileSystemCache.put("key3", "test3");
        fileSystemCache.put("key4", "test4");

        assertTrue(!fileSystemCache.containsKey("key3"));
        assertEquals(CAPACITY, dir.listFiles().length);
    }


    @Test
    public void testGet() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");

        assertEquals("test2", fileSystemCache.get("key2"));
    }

    @Test
    public void testGetNullKey() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");

        assertEquals(null, fileSystemCache.get(null));
    }

    @Test
    public void testContainsKeyTrue() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");

        assertTrue(fileSystemCache.containsKey("key1"));
    }

    @Test
    public void testContainsKeyFalse() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");

        assertTrue(!fileSystemCache.containsKey("key3"));
    }

    @Test
    public void testGetSize() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");

        assertEquals(2, fileSystemCache.getSize());
    }

    @Test
    public void testRemove() {
        fileSystemCache = new FileSystemCache<>(LRU, CAPACITY);

        fileSystemCache.put("key1", "test1");
        fileSystemCache.put("key2", "test2");
        fileSystemCache.remove("key1");

        assertTrue(!fileSystemCache.containsKey("key1"));
        assertEquals(1, dir.listFiles().length);
    }
}
