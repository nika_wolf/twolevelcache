package com.vvolkova.twolevelcache;

import com.vvolkova.twolevelcache.cache.cachelevel.TwoLevelCache;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static com.vvolkova.twolevelcache.strategy.CacheStrategyType.LRU;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TwoLevelCacheTest {
    private static final int LEVEL2_CACHE_CAPACITY = 3;
    private static final String DIR = "cache";
    private TwoLevelCache<String, String> cache;
    private File dir = new File(DIR);

    @Before
    public void init() throws Exception {
        cache = new TwoLevelCache<>(2,3, LRU);
    }

    @Test
    public void testMovingFromCache1ToCache2() {
        cache.put("key1", "test_1");
        cache.put("key2", "test_2");
        cache.put("key3", "test_3");
        cache.put("key4", "test_4");
        cache.get("key1");
        cache.put("key5", "test_5");

        assertTrue(cache.getMemoryCache().containsKey("key5"));
        assertTrue(cache.getMemoryCache().containsKey("key1"));
        assertTrue(cache.getFileSystemCache().containsKey("key2"));
        assertTrue(cache.getFileSystemCache().containsKey("key3"));
        assertTrue(cache.getFileSystemCache().containsKey("key4"));
        assertEquals(LEVEL2_CACHE_CAPACITY, dir.listFiles().length);
    }

    @Test
    public void testPut() {
        cache.put("key1", "test_1");
        cache.put("key2", "test_2");

        assertTrue(cache.getMemoryCache().containsKey("key1"));
        assertTrue(cache.getMemoryCache().containsKey("key2"));
    }

    @Test
    public void testGet() {
        cache.put("key1", "test_1");
        cache.put("key2", "test_2");

        assertEquals("test_1", cache.get("key1"));
    }
}
